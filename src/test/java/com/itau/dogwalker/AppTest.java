package com.itau.dogwalker;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {
	
	
	
	
	
	public static void main (String [] args) {
		
		boolean[]diasSemana = {true,false,true,false,true};
		int frequencia = 3;
		
		List<DayOfWeek> diasAagendar = new ArrayList<>();
		
		for (int i =0; i <  diasSemana.length; i++) {
		
			if( diasSemana[i] == true) {
				
				switch(i) {
				case 0: diasAagendar.add(DayOfWeek.MONDAY);
				break;
				case 1: diasAagendar.add(DayOfWeek.TUESDAY);
				break;
				case 2: diasAagendar.add(DayOfWeek.WEDNESDAY);
				break;
				case 3: diasAagendar.add(DayOfWeek.THURSDAY);
				break;
				case 4: diasAagendar.add(DayOfWeek.FRIDAY);
				break;
				}
			}			
		}
		LocalDate datahoje = LocalDate.now();
		LocalTime hora = LocalTime.now().plusHours(2);
		LocalDateTime hoje = LocalDateTime.of(datahoje, hora);
		
		for (int i =0; i <  diasAagendar.size(); i++) {
			
			
			LocalDateTime proximaData = hoje.with(TemporalAdjusters.next(diasAagendar.get(i)));
			
			System.out.println("ProximaData: " + proximaData);
			
		}
	}
}
