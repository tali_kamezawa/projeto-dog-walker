package com.itau.dogwalker.pojo;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class AnimalRequest {
	
	/**
	 { "cpf": "1031923872",
	   "nome": "Toby",
	   "raca": "bacet",
	   "dataNasc": "2016-02-11"
	   }
	 */
	
	@NotNull
	private String cpf;
	
	@NotNull
	private String nome;

	@NotNull
	private String raca;

	@NotNull
	private LocalDate dataNasc;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

}
