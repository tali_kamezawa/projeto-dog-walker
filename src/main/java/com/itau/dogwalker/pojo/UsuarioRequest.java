package com.itau.dogwalker.pojo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class UsuarioRequest {

	@NotNull
	@Email
	private String email;
	
	@NotNull
	private String senha;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
