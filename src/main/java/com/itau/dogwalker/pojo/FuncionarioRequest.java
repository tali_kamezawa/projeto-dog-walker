package com.itau.dogwalker.pojo;

import java.time.LocalDate;

public class FuncionarioRequest {

	private long id_funcionario;

	private String cpf;
	
	private String nome;
	
	private String sobrenome;
	
	private String email;
	
	private LocalDate dataNasc;
	
	private String senha;
	
	private double perc_repasse;
	
	private String msg_retorno;

	public long getId_funcionario() {
		return id_funcionario;
	}

	public void setId_funcionario(long id_funcionario) {
		this.id_funcionario = id_funcionario;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public double getPerc_repasse() {
		return perc_repasse;
	}

	public void setPerc_repasse(double perc_repasse) {
		this.perc_repasse = perc_repasse;
	}

	public String getMsg_retorno() {
		return msg_retorno;
	}

	public void setMsg_retorno(String msg_retorno) {
		this.msg_retorno = msg_retorno;
	}
	
}
