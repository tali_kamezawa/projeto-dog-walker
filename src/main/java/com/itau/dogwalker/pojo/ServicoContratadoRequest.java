package com.itau.dogwalker.pojo;

import java.time.LocalTime;
import java.util.List;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.itau.dogwalker.model.Animal;
import com.itau.dogwalker.model.Cliente;
import com.itau.dogwalker.utils.TipoServicoEnum;

public class ServicoContratadoRequest {

	/**
	 * 
	{"cpfCliente": "",
	"cpfFuncionario": "",
	"valor" : 20.0,
	"tipoServico": "PASSEIO",
	"horarioFixo": "14:00",
	"diasSemana" [ true, false, true, false, true
	              ],
	"listaAnimaisServicoContratado": null,
	"recorrente": true,
	"ativo": true}
	
	*/
	
	@NotNull
	private String cpfCliente;
	
	@NotNull
	private String cpfFuncionario;
	
	private double valor;
	
	@NotNull
	private TipoServicoEnum tipoServico;
	
	@NotNull
	private LocalTime horarioFixo;
	
	private boolean[]diasSemana = {false,false,false,false,false};
	
	private List<Animal> listaAnimaisServicoContratado;
	
	private boolean recorrente;
	
	private boolean ativo;

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getCpfFuncionario() {
		return cpfFuncionario;
	}

	public void setCpfFuncionario(String cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public TipoServicoEnum getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(TipoServicoEnum tipoServico) {
		this.tipoServico = tipoServico;
	}

	public LocalTime getHorarioFixo() {
		return horarioFixo;
	}

	public void setHorarioFixo(LocalTime horarioFixo) {
		this.horarioFixo = horarioFixo;
	}

	public boolean[] getDiasSemana() {
		return diasSemana;
	}

	public void setDiasSemana(boolean[] diasSemana) {
		this.diasSemana = diasSemana;
	}

	public List<Animal> getListaAnimaisServicoContratado() {
		return listaAnimaisServicoContratado;
	}

	public void setListaAnimaisServicoContratado(List<Animal> listaAnimaisServicoContratado) {
		this.listaAnimaisServicoContratado = listaAnimaisServicoContratado;
	}

	public boolean isRecorrente() {
		return recorrente;
	}

	public void setRecorrente(boolean recorrente) {
		this.recorrente = recorrente;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
}
