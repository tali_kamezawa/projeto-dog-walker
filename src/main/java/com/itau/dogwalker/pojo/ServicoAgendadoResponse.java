package com.itau.dogwalker.pojo;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import com.itau.dogwalker.model.Animal;
import com.itau.dogwalker.utils.TipoServicoEnum;

public class ServicoAgendadoResponse {

	private Long idServicoAgendado;

	private long idFuncionario;
	private String cpfFuncionario;
	private String nomeFuncionario;
	private String sobrenomeFuncionario;

	//O tipo de servico cadastrado está no servicoContratado
	// A ListaDeAnimaisAgendados está no servicoContratado.getListaAnimaisServicoContratado()
	private long idServicoCadastrado;
	private double valorServicoCadastrado;
	private TipoServicoEnum tipoServicoCadastrado;
	private boolean recorrenteServicoCadastrado;
	private boolean ativoServicoCadastrado;

	private List<Animal> listaAnimaisServicoContratado;

	private long idCliente;
	private String cpfCliente;
	private String nomeCliente;

	private LocalDateTime inicioPrevisto;
	private LocalDateTime fimPrevisto;
	private LocalDateTime inicioEfetivo;
	private LocalDateTime fimEfetivo;

	private boolean	doneServicoAgendado;

	public Long getIdServicoAgendado() {
		return idServicoAgendado;
	}

	public void setIdServicoAgendado(Long idServicoAgendado) {
		this.idServicoAgendado = idServicoAgendado;
	}

	public long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getCpfFuncionario() {
		return cpfFuncionario;
	}

	public void setCpfFuncionario(String cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public String getSobrenomeFuncionario() {
		return sobrenomeFuncionario;
	}

	public void setSobrenomeFuncionario(String sobrenomeFuncionario) {
		this.sobrenomeFuncionario = sobrenomeFuncionario;
	}

	public long getIdServicoCadastrado() {
		return idServicoCadastrado;
	}

	public void setIdServicoCadastrado(long idServicoCadastrado) {
		this.idServicoCadastrado = idServicoCadastrado;
	}

	public double getValorServicoCadastrado() {
		return valorServicoCadastrado;
	}

	public void setValorServicoCadastrado(double valorServicoCadastrado) {
		this.valorServicoCadastrado = valorServicoCadastrado;
	}

	public TipoServicoEnum getTipoServicoCadastrado() {
		return tipoServicoCadastrado;
	}

	public void setTipoServicoCadastrado(TipoServicoEnum tipoServicoCadastrado) {
		this.tipoServicoCadastrado = tipoServicoCadastrado;
	}

	public boolean isRecorrenteServicoCadastrado() {
		return recorrenteServicoCadastrado;
	}

	public void setRecorrenteServicoCadastrado(boolean recorrenteServicoCadastrado) {
		this.recorrenteServicoCadastrado = recorrenteServicoCadastrado;
	}

	public boolean isAtivoServicoCadastrado() {
		return ativoServicoCadastrado;
	}

	public void setAtivoServicoCadastrado(boolean ativoServicoCadastrado) {
		this.ativoServicoCadastrado = ativoServicoCadastrado;
	}

	public List<Animal> getListaAnimaisServicoContratado() {
		return listaAnimaisServicoContratado;
	}

	public void setListaAnimaisServicoContratado(List<Animal> listaAnimaisServicoContratado) {
		this.listaAnimaisServicoContratado = listaAnimaisServicoContratado;
	}

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public LocalDateTime getInicioPrevisto() {
		return inicioPrevisto;
	}

	public void setInicioPrevisto(LocalDateTime inicioPrevisto) {
		this.inicioPrevisto = inicioPrevisto;
	}

	public LocalDateTime getFimPrevisto() {
		return fimPrevisto;
	}

	public void setFimPrevisto(LocalDateTime fimPrevisto) {
		this.fimPrevisto = fimPrevisto;
	}

	public LocalDateTime getInicioEfetivo() {
		return inicioEfetivo;
	}

	public void setInicioEfetivo(LocalDateTime inicioEfetivo) {
		this.inicioEfetivo = inicioEfetivo;
	}

	public LocalDateTime getFimEfetivo() {
		return fimEfetivo;
	}

	public void setFimEfetivo(LocalDateTime fimEfetivo) {
		this.fimEfetivo = fimEfetivo;
	}

	public boolean isDoneServicoAgendado() {
		return doneServicoAgendado;
	}

	public void setDoneServicoAgendado(boolean doneServicoAgendado) {
		this.doneServicoAgendado = doneServicoAgendado;
	}

	
	
}
