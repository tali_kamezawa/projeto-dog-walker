package com.itau.dogwalker.pojo;

import java.util.List;

import com.itau.dogwalker.model.ServicoAgendado;
import com.itau.dogwalker.model.ServicoCadastrado;

public class ServicoCadastradoResponse {

	private ServicoCadastrado servicoCadastrado;
	
	private List <ServicoAgendado> servicosAgendados;

	public ServicoCadastrado getServicoCadastrado() {
		return servicoCadastrado;
	}

	public void setServicoCadastrado(ServicoCadastrado servicoCadastrado) {
		this.servicoCadastrado = servicoCadastrado;
	}

	public List<ServicoAgendado> getServicosAgendados() {
		return servicosAgendados;
	}

	public void setServicosAgendados(List<ServicoAgendado> servicosAgendados) {
		this.servicosAgendados = servicosAgendados;
	}
}
