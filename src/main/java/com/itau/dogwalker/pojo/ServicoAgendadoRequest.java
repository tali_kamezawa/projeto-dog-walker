package com.itau.dogwalker.pojo;

import java.time.LocalDate;

public class ServicoAgendadoRequest {

	/**
	 {
		"cpfFuncionario": "11123412337",
		"dataServicoAgendado": "2018-08-06"
	}
	 */
	private String cpfFuncionario;
	private LocalDate dataServicoAgendado;
	public String getCpfFuncionario() {
		return cpfFuncionario;
	}
	public void setCpfFuncionario(String cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}
	public LocalDate getDataServicoAgendado() {
		return dataServicoAgendado;
	}
	public void setDataServicoAgendado(LocalDate dataServicoAgendado) {
		this.dataServicoAgendado = dataServicoAgendado;
	}
	
}
