package com.itau.dogwalker.pojo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.itau.dogwalker.model.Animal;

public class CadastroResponse implements Serializable{

	private static final long serialVersionUID = -3383565812512307062L;
	
	private long id_pessoa;

	private String cpf;
	
	private String nome;
	
	private String sobrenome;
	
	private String email;
	
	private LocalDate dataNasc;
	
	private String senha;
	
	List<Animal> listaDeAnimais;

	public long getId_pessoa() {
		return id_pessoa;
	}

	public void setId_pessoa(long id_pessoa) {
		this.id_pessoa = id_pessoa;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Animal> getListaDeAnimais() {
		return listaDeAnimais;
	}

	public void setListaDeAnimais(List<Animal> listaDeAnimais) {
		this.listaDeAnimais = listaDeAnimais;
	}
	
	
	
}
