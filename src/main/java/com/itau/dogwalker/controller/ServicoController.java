package com.itau.dogwalker.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.dogwalker.model.Animal;
import com.itau.dogwalker.model.Cliente;
import com.itau.dogwalker.model.Funcionario;
import com.itau.dogwalker.model.ServicoAgendado;
import com.itau.dogwalker.model.ServicoCadastrado;
import com.itau.dogwalker.pojo.AnimalRequest;
import com.itau.dogwalker.pojo.FuncionarioRequest;
import com.itau.dogwalker.pojo.ServicoAgendadoRequest;
import com.itau.dogwalker.pojo.ServicoAgendadoResponse;
import com.itau.dogwalker.pojo.ServicoCadastradoResponse;
import com.itau.dogwalker.pojo.ServicoContratadoRequest;
import com.itau.dogwalker.repository.AnimalRepository;
import com.itau.dogwalker.repository.ClienteRespository;
import com.itau.dogwalker.repository.FuncionarioRepository;
import com.itau.dogwalker.repository.ServicoCadastradoRepository;
import com.itau.dogwalker.service.PasswordService;
import com.itau.dogwalker.service.ServicoService;

@RestController
@RequestMapping("/servico")
@CrossOrigin
public class ServicoController {

	@Autowired private PasswordService passwordService;

	@Autowired private ClienteRespository clienteRepository;
	@Autowired private FuncionarioRepository funcionarioRepository;
	
	@Autowired private ServicoCadastradoRepository servicoCadastradoRepository;
	
	@Autowired private ServicoService servicoService;
	
	@Autowired private AnimalRepository animalRepository;

	@RequestMapping(method=RequestMethod.POST, path="/cadastro/contratado")
	public ResponseEntity<ServicoCadastradoResponse> cadastrarServicoContratado(@RequestBody ServicoContratadoRequest novoServicoCad) {

		
		Cliente cliente =  clienteRepository.findByCpf(novoServicoCad.getCpfCliente());
		
		Funcionario funcionario = funcionarioRepository.findByCpf(novoServicoCad.getCpfFuncionario());
		
		if(null == cliente || null == funcionario) {
			return ResponseEntity.notFound().build();
		}
		
		ServicoCadastradoResponse servico = servicoService.salvar(novoServicoCad, cliente, funcionario);

		return ResponseEntity.ok().body(servico);

	}
	
	@RequestMapping(method=RequestMethod.GET, path="/servicoAgendado/{cpfFuncionario}/{dataServicoAgendado}")
	public ResponseEntity<List<ServicoAgendado>> buscarServicoAgendado (@PathVariable String cpfFuncionario,
			@PathVariable(value="dataServicoAgendado") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataServicoAgendado){
		
		Funcionario funcionario = funcionarioRepository.findByCpf(cpfFuncionario);
		if(null == funcionario) {
			return ResponseEntity.notFound().build();
		}
		
		List<ServicoAgendado> servicoAgendado = servicoService.buscarServicosAgendados(dataServicoAgendado, funcionario);
		
		return ResponseEntity.ok().body(servicoAgendado);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/servicoAgendado/buscar/{cpfFuncionario}/{dataServicoAgendado}")
	public ResponseEntity<List<ServicoAgendadoResponse>> buscarServicoAgendadoResponse (@PathVariable String cpfFuncionario,
			@PathVariable(value="dataServicoAgendado") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataServicoAgendado){
		
		Funcionario funcionario = funcionarioRepository.findByCpf(cpfFuncionario);
		if(null == funcionario) {
			return ResponseEntity.notFound().build();
		}
		
		List<ServicoAgendadoResponse> servicoAgendado = servicoService.buscarServicosAgendadosResponse(dataServicoAgendado, funcionario);
		
		return ResponseEntity.ok().body(servicoAgendado);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/buscar/contratado")
	public ResponseEntity<ServicoCadastrado> buscarServicoContratado(@RequestBody ServicoContratadoRequest novoServicoCad) {

		
		Cliente cliente =  clienteRepository.findByCpf(novoServicoCad.getCpfCliente());
		Funcionario funcionario = funcionarioRepository.findByCpf(novoServicoCad.getCpfFuncionario());
		if(null == cliente || null == funcionario) {
			return ResponseEntity.notFound().build();
		}
		
//		ServicoCadastrado servico = servicoService.salvar(novoServicoCad, cliente, funcionario);
//		
//		if(null == servico || servico.equals(null)) {
//			return ResponseEntity.notFound().build();
//		}
//		
//		ServicoCadastrado salvo = servicoCadastradoRepository.save(servico);

		return null; //ResponseEntity.ok().body(salvo);

	}

	


}
