package com.itau.dogwalker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.dogwalker.model.Animal;
import com.itau.dogwalker.model.Cliente;
import com.itau.dogwalker.pojo.AnimalRequest;
import com.itau.dogwalker.repository.AnimalRepository;
import com.itau.dogwalker.repository.ClienteRespository;
import com.itau.dogwalker.service.PasswordService;

@RestController
@RequestMapping("/cadastro")
public class CadastroController {

	@Autowired
	PasswordService passwordService;

	@Autowired
	ClienteRespository clienteRepository;
	
	@Autowired
	AnimalRepository animalRepository;

	@RequestMapping(method=RequestMethod.POST, path="/cliente")
	public ResponseEntity<Cliente> cadastrarCliente(@RequestBody Cliente novoCliente) {

		novoCliente.setCpf(novoCliente.getCpf());
		novoCliente.setNome(novoCliente.getNome());

		if(null != novoCliente.getSenha()) {
			String hash = passwordService.gerarHash(novoCliente.getSenha());
			novoCliente.setSenha(hash);
		}

		if(null != novoCliente.getListaDeAnimais()) {
			for(Animal ani : novoCliente.getListaDeAnimais()) {
				animalRepository.save(ani);
			}

		}

		Cliente cliente = clienteRepository.save(novoCliente);

		return ResponseEntity.ok().body(cliente);

	}

	@RequestMapping(method=RequestMethod.POST, path="/cliente/inserirAnimal")
	public ResponseEntity<Cliente> inserirAnimal(@RequestBody AnimalRequest animal) {

		Cliente optionalProduto = clienteRepository.findByCpf(animal.getCpf());

		if(null == optionalProduto) {
			return ResponseEntity.notFound().build();
		}

		Animal novoAnimal = new Animal(); 
		novoAnimal.setNome(animal.getNome());
		novoAnimal.setRaca(animal.getRaca());
		novoAnimal.setDataNasc(animal.getDataNasc());
		animalRepository.save(novoAnimal);
		
		optionalProduto.getListaDeAnimais().add(novoAnimal);

		Cliente cliente = new Cliente();
		cliente = clienteRepository.save(optionalProduto);
		
		return ResponseEntity.ok().body(cliente);

	}
	
	@RequestMapping(method=RequestMethod.POST, path="/cliente/atualizar")
	public ResponseEntity<Cliente> atualizarCliente(@RequestBody Cliente novoCliente) {

		Cliente optionalProduto = clienteRepository.findByCpf(novoCliente.getCpf());

		if(null == optionalProduto) {
			return ResponseEntity.notFound().build();
		}


		novoCliente.setCpf(novoCliente.getCpf());
		novoCliente.setNome(novoCliente.getNome());
		novoCliente.setSobrenome(novoCliente.getSobrenome());
		novoCliente.setListaDeAnimais(novoCliente.getListaDeAnimais());


		String hash = passwordService.gerarHash(novoCliente.getSenha());
		novoCliente.setSenha(hash);

		Cliente cliente = new Cliente();
		cliente = clienteRepository.save(novoCliente);
		return ResponseEntity.ok().body(cliente);

	}
}
