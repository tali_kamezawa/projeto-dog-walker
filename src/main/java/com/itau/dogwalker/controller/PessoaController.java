package com.itau.dogwalker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.dogwalker.model.Pessoa;
import com.itau.dogwalker.pojo.UsuarioRequest;
import com.itau.dogwalker.repository.AnimalRepository;
import com.itau.dogwalker.repository.PessoaRepository;
import com.itau.dogwalker.service.JwtService;
import com.itau.dogwalker.service.PasswordService;

@RestController
@RequestMapping("/pessoaLogin")
@CrossOrigin
public class PessoaController {


	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JwtService jwtService;

	@Autowired
	PessoaRepository pessoaRepository;
	
	@Autowired
	AnimalRepository animalRepository;

	@RequestMapping(method=RequestMethod.POST, path="/buscar")
	public ResponseEntity<Pessoa> validarUsuario(@RequestBody UsuarioRequest user) {

		Pessoa optionalpessoa = pessoaRepository.findByEmail(user.getEmail());
		
		String hash = passwordService.gerarHash(user.getSenha());
	
		if(passwordService.verificarHash(user.getSenha(), optionalpessoa.getSenha())) {
			// o token será baseado no email
			String token = jwtService.gerarToken(user.getEmail());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<Pessoa>(optionalpessoa, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.notFound().build();

	}

}
