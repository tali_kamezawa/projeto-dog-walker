package com.itau.dogwalker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.dogwalker.model.Funcionario;
import com.itau.dogwalker.pojo.FuncionarioRequest;
import com.itau.dogwalker.model.Funcionario;
import com.itau.dogwalker.repository.FuncionarioRepository;
import com.itau.dogwalker.service.PasswordService;

@RestController
@RequestMapping("/cadastro")
@CrossOrigin
public class FuncionarioController {

	@Autowired
	FuncionarioRepository funcionarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@RequestMapping(method=RequestMethod.POST, path="/funcionario")
	public ResponseEntity<?> cadastrarFuncionario(@RequestBody Funcionario novoFuncionario) {

//		novoFuncionario.setCpf(novoFuncionario.getCpf());
//		novoFuncionario.setNome(novoFuncionario.getNome());

		Funcionario optionalFuncionario = funcionarioRepository.findByCpf(novoFuncionario.getCpf());

		//criar um novo objeto FuncionarioRequest
		
		if(null != optionalFuncionario) {
			FuncionarioRequest funcionariorequest = new FuncionarioRequest();
			funcionariorequest.setMsg_retorno("JA EXISTE UM FUNCIONARIO COM ESTE CPF");
			return ResponseEntity.ok().body(funcionariorequest);
//			return ResponseEntity.notFound().build();
		}
		
		if(null != novoFuncionario.getSenha()) {
			String hash = passwordService.gerarHash(novoFuncionario.getSenha());
			novoFuncionario.setSenha(hash);
		}

		Funcionario funcionario = funcionarioRepository.save(novoFuncionario);

		//mover as variaveis de um objeto para o outro (nao acha o id da pessoa, pq o id nao esta em funcionario)
//		funcionariorequest.setId_funcionario(999);
//		funcionariorequest.setCpf(funcionario.getCpf());
//		funcionariorequest.setDataNasc(funcionario.getDataNasc());
//		funcionariorequest.setEmail(funcionario.getEmail());
//		funcionariorequest.setNome(funcionario.getNome());
//		funcionariorequest.setSobrenome(funcionario.getSobrenome());
//		funcionariorequest.setPerc_repasse(funcionario.getPercRepasse());
//		funcionariorequest.setSenha(funcionario.getSenha());
//		funcionariorequest.setId_funcionario(funcionario.getId_pessoa());

		return ResponseEntity.ok().body(funcionario);

	}

	@RequestMapping(method=RequestMethod.POST, path="/funcionario/atualizar")
	public ResponseEntity<Funcionario> atualizarFuncionario(@RequestBody Funcionario novoFuncionario) {

		Funcionario optionalFuncionario = funcionarioRepository.findByCpf(novoFuncionario.getCpf());

		if(null == optionalFuncionario) {
			return ResponseEntity.notFound().build();
		}

		novoFuncionario.setCpf(novoFuncionario.getCpf());
		novoFuncionario.setNome(novoFuncionario.getNome());
		novoFuncionario.setSobrenome(novoFuncionario.getSobrenome());

		String hash = passwordService.gerarHash(novoFuncionario.getSenha());
		novoFuncionario.setSenha(hash);

		Funcionario funcionario = new Funcionario();
		funcionario = funcionarioRepository.save(novoFuncionario);
		return ResponseEntity.ok().body(funcionario);

	}

}
