package com.itau.dogwalker.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public String gerarHash(String senha) {
		return encoder.encode(senha);
	}

	public boolean verificarHash(String senha, String hash) {
		return encoder.matches(senha, hash);
	}
}
