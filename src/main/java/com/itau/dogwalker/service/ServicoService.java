package com.itau.dogwalker.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.dogwalker.model.Animal;
import com.itau.dogwalker.model.Cliente;
import com.itau.dogwalker.model.Funcionario;
import com.itau.dogwalker.model.ServicoAgendado;
import com.itau.dogwalker.model.ServicoCadastrado;
import com.itau.dogwalker.pojo.ServicoAgendadoRequest;
import com.itau.dogwalker.pojo.ServicoAgendadoResponse;
import com.itau.dogwalker.pojo.ServicoCadastradoResponse;
import com.itau.dogwalker.pojo.ServicoContratadoRequest;
import com.itau.dogwalker.repository.AnimalRepository;
import com.itau.dogwalker.repository.ServicoAgendadoRepository;
import com.itau.dogwalker.repository.ServicoCadastradoRepository;
import com.itau.dogwalker.utils.TipoServicoEnum;


@Service
public class ServicoService {

	@Autowired private ServicoCadastradoRepository servicoCadastradoRepository;
	@Autowired private ServicoAgendadoRepository servicoAgendadoRepository;
	@Autowired private AnimalRepository animalRepository;

	public ServicoCadastradoResponse salvar(ServicoContratadoRequest novoServicoCad, Cliente cliente, Funcionario funcionario) {

		ServicoCadastradoResponse resultado = new ServicoCadastradoResponse();
		ServicoCadastrado servico = new ServicoCadastrado();

		servico.setAtivo(novoServicoCad.isAtivo());
		servico.setCliente(cliente);
		servico.setFuncionario(funcionario);
		servico.setHorarioFixo(novoServicoCad.getHorarioFixo());
		servico.setRecorrente(novoServicoCad.isRecorrente());
		servico.setTipoServico(TipoServicoEnum.valueOf(novoServicoCad.getTipoServico().toString()));
		servico.setValor(novoServicoCad.getValor());

		if(null != novoServicoCad.getDiasSemana()) {
			int frequencia =0;
			for (boolean dia : novoServicoCad.getDiasSemana()) {
				if(dia == true) {
					frequencia++;
				}
			}
			servico.setFrequencia(frequencia);
		}

		servico.setDiasSemana(novoServicoCad.getDiasSemana());

		if(null == novoServicoCad.getListaAnimaisServicoContratado()) {
			ArrayList<Animal> lista = new ArrayList<>();

			for(Animal animal: cliente.getListaDeAnimais()) {
				lista.add(animal);
			}

			servico.setListaAnimaisServicoContratado(lista);
		}
		else {
			for(Animal ani : novoServicoCad.getListaAnimaisServicoContratado()) {
				animalRepository.save(ani);
			}
			//servico.setListaAnimaisServicoContratado();
		}

		ServicoCadastrado salvo = servicoCadastradoRepository.save(servico);
		if(null == salvo || salvo.equals(null)) {
			return null;
		}
		List <ServicoAgendado> servicosAgendados = criarServicosAgendados(salvo);

		for(ServicoAgendado ser : servicosAgendados) {
			servicoAgendadoRepository.save(ser);
		}

		resultado.setServicoCadastrado(salvo);
		resultado.setServicosAgendados(servicosAgendados);
		return resultado;

	}

	public List <ServicoAgendado> criarServicosAgendados(ServicoCadastrado servicoCadastrado){

		List <ServicoAgendado> resultado = new ArrayList<>();

		// recupero quais dias da semana contratados
		boolean[]diasSemana = servicoCadastrado.getDiasSemana();
		List<DayOfWeek> diasAagendar = diasAagendar(diasSemana);

		LocalDate datahoje = LocalDate.now();
		LocalTime hora = servicoCadastrado.getHorarioFixo();
		LocalDateTime hoje = LocalDateTime.of(datahoje, hora);

		for (int i =0; i <  diasAagendar.size(); i++) {
			LocalDateTime proximaData = hoje.with(TemporalAdjusters.next(diasAagendar.get(i)));
			System.out.println("ProximaData: " + proximaData);
			ServicoAgendado agendado = gerarServico(servicoCadastrado, proximaData);
			resultado.add(agendado);	 
		}
		return resultado;
	}

	public  List <ServicoAgendado> buscarServicosAgendados( LocalDate dataServicoAgendado, Funcionario func){

		List <ServicoAgendado> listaServicosPorFunc = new ArrayList<>();

		LocalTime horainicio = LocalTime.of(0, 0, 0);
		LocalDateTime inicio = LocalDateTime.of(dataServicoAgendado, horainicio);

		LocalTime horafim = LocalTime.of(23, 59, 59);
		LocalDateTime fim = LocalDateTime.of(dataServicoAgendado, horafim);

		listaServicosPorFunc = servicoAgendadoRepository.findServicosAgendadosPorFuncionario(func.getId_pessoa(),
				inicio, fim);
		return listaServicosPorFunc;

	}

	public  List <ServicoAgendadoResponse> buscarServicosAgendadosResponse( LocalDate dataServicoAgendado, Funcionario func){

		List <ServicoAgendadoResponse> listaServicosPorFunc = new ArrayList<>();

		LocalTime horainicio = LocalTime.of(0, 0, 0);
		LocalDateTime inicio = LocalDateTime.of(dataServicoAgendado, horainicio);

		LocalTime horafim = LocalTime.of(23, 59, 59);
		LocalDateTime fim = LocalDateTime.of(dataServicoAgendado, horafim);

		List <ServicoAgendado> listaServicosAgendados = servicoAgendadoRepository.findServicosAgendadosPorFuncionario(func.getId_pessoa(), inicio, fim);
		
		for (ServicoAgendado serv : listaServicosAgendados) {
			ServicoAgendadoResponse servico = populaServicosAgendadosResponse(serv);
			listaServicosPorFunc.add(servico);	
		}
		
		
		return listaServicosPorFunc;

	}
	private ServicoAgendadoResponse populaServicosAgendadosResponse(ServicoAgendado agendado) {
		
		ServicoAgendadoResponse servico = new ServicoAgendadoResponse();
		
		servico.setIdServicoAgendado(agendado.getIdServicoAgendado());
		servico.setAtivoServicoCadastrado(agendado.getServicoCadastrado().isAtivo());
		servico.setDoneServicoAgendado(agendado.isDone());
		servico.setInicioPrevisto(agendado.getInicioPrevisto());
		servico.setFimPrevisto(agendado.getFimPrevisto());
		servico.setInicioEfetivo(null);
		servico.setFimEfetivo(null);
		
		servico.setIdCliente(agendado.getServicoCadastrado().getCliente().getId_pessoa());
		servico.setNomeCliente(agendado.getServicoCadastrado().getCliente().getNome());
		servico.setCpfCliente(agendado.getServicoCadastrado().getCliente().getCpf());
		servico.setListaAnimaisServicoContratado(agendado.getServicoCadastrado().getCliente().getListaDeAnimais());
		
		servico.setIdFuncionario(agendado.getServicoCadastrado().getFuncionario().getId_pessoa());
		servico.setNomeFuncionario(agendado.getServicoCadastrado().getFuncionario().getNome());
		servico.setCpfFuncionario(agendado.getServicoCadastrado().getFuncionario().getCpf());
		servico.setSobrenomeFuncionario(agendado.getServicoCadastrado().getFuncionario().getSobrenome() );
		
		servico.setIdServicoCadastrado(agendado.getServicoCadastrado().getIdServico());
		servico.setRecorrenteServicoCadastrado(agendado.getServicoCadastrado().isRecorrente());
		servico.setTipoServicoCadastrado(agendado.getServicoCadastrado().getTipoServico());
		servico.setValorServicoCadastrado(agendado.getServicoCadastrado().getValor());
		return servico;
	}

	private ServicoAgendado gerarServico(ServicoCadastrado servicoCadastrado, LocalDateTime proximaData) {
		ServicoAgendado agendado = new ServicoAgendado();

		agendado.setInicioPrevisto(proximaData);
		agendado.setServicoCadastrado(servicoCadastrado);

		agendado.setFimPrevisto(proximaData.plusHours(1));
		agendado.setFuncionario(servicoCadastrado.getFuncionario());
		//agendado.setIdServicoAgendado(servicoCadastrado.getIdServico());
		agendado.setInicioEfetivo(null);
		agendado.setFimEfetivo(null);
		agendado.setDone(false);


		return agendado;
	}

	private List<DayOfWeek> diasAagendar(boolean[]diasSemana){
		List<DayOfWeek> diasAagendar = new ArrayList<>();

		for (int i =0; i <  diasSemana.length; i++) {

			if( diasSemana[i] == true) {

				switch(i) {
				case 0: diasAagendar.add(DayOfWeek.MONDAY);
				break;
				case 1: diasAagendar.add(DayOfWeek.TUESDAY);
				break;
				case 2: diasAagendar.add(DayOfWeek.WEDNESDAY);
				break;
				case 3: diasAagendar.add(DayOfWeek.THURSDAY);
				break;
				case 4: diasAagendar.add(DayOfWeek.FRIDAY);
				break;
				}
			}			
		}

		return diasAagendar;
	}

}
