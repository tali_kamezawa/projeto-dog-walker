package com.itau.dogwalker.service;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JwtService {

	String key = "gatonet";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String email) {
		return JWT.create().withClaim("email", email).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("email").asString();
		}catch (Exception e) {
			return null;
		}
	}

}
