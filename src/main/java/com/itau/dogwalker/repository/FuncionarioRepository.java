package com.itau.dogwalker.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.dogwalker.model.Funcionario;

public interface FuncionarioRepository extends CrudRepository<Funcionario, String>{

	public Funcionario findByCpf(String cpf);
}


