package com.itau.dogwalker.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.dogwalker.model.Cliente;


@Repository
public interface ClienteRespository extends CrudRepository<Cliente, String>{

	public Cliente findByCpf(String cpf);
}
