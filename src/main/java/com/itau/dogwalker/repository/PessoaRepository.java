package com.itau.dogwalker.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.dogwalker.model.Pessoa;
@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, String>{

	public Pessoa findByEmail(String email);
}
