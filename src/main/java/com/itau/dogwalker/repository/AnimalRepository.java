package com.itau.dogwalker.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.dogwalker.model.Animal;


@Repository
public interface AnimalRepository extends CrudRepository<Animal, Long>{

	public Animal findByIdAnimal(Long id);
	
}
