package com.itau.dogwalker.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.dogwalker.model.ServicoCadastrado;

@Repository
public interface ServicoCadastradoRepository extends CrudRepository<ServicoCadastrado, Long>{

}
