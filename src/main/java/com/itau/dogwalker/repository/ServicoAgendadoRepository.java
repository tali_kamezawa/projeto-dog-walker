package com.itau.dogwalker.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Session;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.itau.dogwalker.model.ServicoAgendado;

@Repository
public interface ServicoAgendadoRepository extends CrudRepository<ServicoAgendado, Long>{
	
	//SELECT * FROM servico_agendado s WHERE s.funcionario_id_pessoa = 1 
	// s.inicio_previsto BETWEEN  '2018-08-03 00:00:01' AND '2018-08-03 23:00:00';
	//
	//@Query(value="SELECT s FROM ServicoAgendado s WHERE s.inicioPrevisto BETWEEN :inicio AND :fim AND s.id_pessoa = :idPessoa ")
			//+ " AND s.inicioPrevisto BETWEEN :inicio AND :fim  ")
//	@Query(value="SELECT s FROM ServicoAgendado s  WHERE s.inicioPrevisto BETWEEN :inicio AND :fim ")
	@Query(value="SELECT s FROM ServicoAgendado s INNER JOIN s.funcionario f WHERE s.inicioPrevisto BETWEEN :inicio AND :fim AND f.id_pessoa"
			+ " = :idPessoa")
	public List <ServicoAgendado> findServicosAgendadosPorFuncionario(@Param("idPessoa") long idPessoa, 
														@Param("inicio") LocalDateTime inicio, 
														@Param("fim") LocalDateTime fim);	
	
//	Session session;
//	public  List <ServicoAgendado> busca( ServicoAgendado servicoAgendado){
//		String hql = "from ServicoAgendado s where s.id_pessoa = :idPessoa and s.inicioPrevisto BETWEEN :inicio AND :fim";
//		
//		return session.createQuery(hql)
//				.setParameter("idPessoa", servicoAgendado.getFuncionario().getId_pessoa())
//				.setParameter( "inicio", servicoAgendado.getInicioPrevisto())
//				.setParameter("fim", servicoAgendado.getFimPrevisto())
//				.list();
//	}
	
}
