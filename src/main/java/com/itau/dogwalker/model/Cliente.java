package com.itau.dogwalker.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Cliente extends Pessoa{

	@OneToMany
	List<Animal> listaDeAnimais;

	public List<Animal> getListaDeAnimais() {
		return listaDeAnimais;
	}

	public void setListaDeAnimais(List<Animal> listaDeAnimais) {
		this.listaDeAnimais = listaDeAnimais;
	}

}
