package com.itau.dogwalker.model;

import java.time.LocalTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ManyToAny;

import com.itau.dogwalker.utils.TipoServicoEnum;

@Entity
public class ServicoCadastrado {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idServico;
	
	private double valor;
	
	@NotNull
	private TipoServicoEnum tipoServico;
	
	@NotNull
	private LocalTime horarioFixo;
	
	@NotNull
	private int frequencia;
	
	private boolean[]diasSemana = {false,false,false,false,false};

	@OneToOne
	private Cliente cliente;
	
	@ManyToOne
	private Funcionario funcionario;
	
	//@OneToMany
	@ManyToMany
	private List<Animal> listaAnimaisServicoContratado;
	
	private boolean recorrente;
	private boolean ativo;
	
	
	
	public long getIdServico() {
		return idServico;
	}
	
	public ServicoCadastrado() {
		super();
		for(boolean dia : diasSemana) {
		dia = false;
		}
	}
	
	public void setIdServico(long idServico) {
		this.idServico = idServico;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public TipoServicoEnum getTipoServico() {
		return tipoServico;
	}
	public void setTipoServico(TipoServicoEnum tipoServico) {
		this.tipoServico = tipoServico;
	}
	public LocalTime getHorarioFixo() {
		return horarioFixo;
	}
	public void setHorarioFixo(LocalTime horarioFixo) {
		this.horarioFixo = horarioFixo;
	}
	public int getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(int frequencia) {
		this.frequencia = frequencia;
	}

	public boolean[] getDiasSemana() {
		return diasSemana;
	}
	public void setDiasSemana(boolean[] diasSemana) {
		this.diasSemana = diasSemana;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<Animal> getListaAnimaisServicoContratado() {
		return listaAnimaisServicoContratado;
	}
	public void setListaAnimaisServicoContratado(List<Animal> listaAnimaisServicoContratado) {
		this.listaAnimaisServicoContratado = listaAnimaisServicoContratado;
	}
	public boolean isRecorrente() {
		return recorrente;
	}
	public void setRecorrente(boolean recorrente) {
		this.recorrente = recorrente;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
