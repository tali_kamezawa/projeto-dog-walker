package com.itau.dogwalker.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Funcionario extends Pessoa{
	
	@NotNull
	private double percRepasse;

	public double getPercRepasse() {
		return percRepasse;
	}

	public void setPercRepasse(double percRepasse) {
		this.percRepasse = percRepasse;
	}

}
