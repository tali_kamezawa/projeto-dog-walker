package com.itau.dogwalker.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class ServicoAgendado {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idServicoAgendado;
	
	@OneToOne
	private Funcionario funcionario;
	
	//O tipo de servico cadastrado está no servicoContratado
	// A ListaDeAnimaisAgendados está no servicoContratado.getListaAnimaisServicoContratado()
	@ManyToOne
	private ServicoCadastrado servicoCadastrado;
	
	private LocalDateTime inicioPrevisto;
	
	private LocalDateTime fimPrevisto;
	
	private LocalDateTime inicioEfetivo;
	
	private LocalDateTime fimEfetivo;
	
	private boolean	done;

	public Long getIdServicoAgendado() {
		return idServicoAgendado;
	}

	public void setIdServicoAgendado(Long idServicoAgendado) {
		this.idServicoAgendado = idServicoAgendado;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public ServicoCadastrado getServicoCadastrado() {
		return servicoCadastrado;
	}

	public void setServicoCadastrado(ServicoCadastrado servicoCadastrado) {
		this.servicoCadastrado = servicoCadastrado;
	}

	public LocalDateTime getInicioPrevisto() {
		return inicioPrevisto;
	}

	public void setInicioPrevisto(LocalDateTime inicioPrevisto) {
		this.inicioPrevisto = inicioPrevisto;
	}

	public LocalDateTime getFimPrevisto() {
		return fimPrevisto;
	}

	public void setFimPrevisto(LocalDateTime fimPrevisto) {
		this.fimPrevisto = fimPrevisto;
	}

	public LocalDateTime getInicioEfetivo() {
		return inicioEfetivo;
	}

	public void setInicioEfetivo(LocalDateTime inicioEfetivo) {
		this.inicioEfetivo = inicioEfetivo;
	}

	public LocalDateTime getFimEfetivo() {
		return fimEfetivo;
	}

	public void setFimEfetivo(LocalDateTime fimEfetivo) {
		this.fimEfetivo = fimEfetivo;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

}
