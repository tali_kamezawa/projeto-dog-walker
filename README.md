# projeto-dog-walker


Para inserir cliente localhost:8080/cadastro/cliente
{
    "cpf": "12023412323",
    "nome": "teste talitha um",
    "sobrenome": "sobrenome",
    "email": "tali@tali.com",
    "dataNasc": "1990-12-10",
    "senha": "$2a$10$SGRX1he6m9A6W3BifwsX1uSquKKPE6SvH.Odv9ofkK7V7pALNyCYu",
    "listaDeAnimais": [
        {
            "nome": "Kika",
            "raca": "shitzu",
            "dataNasc": "2015-03-05"
        },
        {
            "nome": "Marrie",
            "raca": "poodle",
            "dataNasc": "2016-01-31"
        }
    ]
}


Para inserir apenas animal: localhost:8080/cadastro/cliente/inserirAnimal
{ "cpf": "16023412323",
	   "nome": "Toby",
	   "raca": "bacet",
	   "dataNasc": "2016-02-11"
	
}

Para Inserir servico Contratado:  localhost:8080/servico/cadastro/contratado  POST

{
	"cpfCliente": "12023412323",
	"cpfFuncionario": "11123412337",
	"valor" : 110.0,
	"tipoServico": "PASSEIO",
	"horarioFixo": "13:00",
	"diasSemana": [ false, true, false, true, true],
	"listaAnimaisServicoContratado": null,
	"recorrente": true,
	"ativo": true
	
}
	
Para buscar get
passar CPFdo Funcionario:11123412337 e data: 2018-08-03
localhost:8080/servico/servicoAgendado/11123412337/2018-08-03

