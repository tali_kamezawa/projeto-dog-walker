const botaoLogin = document.querySelector("#login")
const inputUsuario = document.querySelector("#usuario");
const inputSenha = document.querySelector("#senha");

botaoLogin.onclick = validarLogin;

let flagErro = false;

function validarLogin() {
    
    if(validarSenha() && validarEmail()){
        var obj = {"email":inputUsuario.value, "senha":inputSenha.value};

        //fetch(`http://localhost:8080/pessoaLogin/buscar/${inputUsuario.value}`).then(extrairJSON).then(validarAcessoSistema);
        fetch("http://178.128.2.85:8080/pessoaLogin/buscar/",
            {
                method: 'post',
                body: JSON.stringify(obj),
                headers: {'content-type': 'application/json'}
            }).then(extrairJSON).then(validarAcessoSistema);
    }
    else{
        alert("Favor preencher corretamente todos os campos.");
    }
}

function extrairJSON(resposta) {
    if (resposta.status != 200) {
        flagErro = true;
    }
    else{
        flagErro = false;
    }

    return resposta.json();
}


function validarSenha(){
    let senha = inputSenha.value;

    if((senha.length >= 6 && senha.match(/[A-Za-z]/) && senha.match(/[0-9]/)) )
        {
            return true;
        }
        return false;
}



function validarEmail(){
    let email = inputUsuario.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");

    if((arroba === ultimoArroba && 
        arroba !== -1 &&
        ultimoPonto - arroba >= 3) ||
        (email === ""))
        {
            return true;
        }
        return false;
}


function validarAcessoSistema(dados) {
    if (flagErro == false) {

        localStorage.setItem('usuario', JSON.stringify(dados));

        if (dados.perfil == "Admin" ) {
            window.location.href = "Agenda.html";
        }
        else{
            window.location.href = "Agenda.html";
        }
    }
    else{
        alert("Usuário inválido ou senha incorreta");
    }  
} 
