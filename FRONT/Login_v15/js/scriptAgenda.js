// Obter usuário da página anterior
let usuario = JSON.parse(localStorage.getItem("usuario"));
document.title = "Agenda - " + usuario.nome + " " + usuario.sobrenome;


// Montar dinamicamente o calendário
const sectionCalendario = document.querySelector("#ulPrincipal");
let liPrincipal = document.createElement("li");
let html = "";
let html2 = "";
liPrincipal.innerHTML = '';
let formato = 1;
let data = new Date();
let dataFormatada = (data.toString()).slice(0,10);
let chamadasAPI = [];

for (let dia = 0; dia < 5; dia++) {
    let param1 = usuario.cpf;
    let ano = data.getFullYear().toString();
    let mes = (data.getMonth()+1).toString();
    if (mes.length == 1) {
        mes = "0" + mes;
    }
    let dia = data.getDate().toString();
    if (dia.length == 1) {
        dia = "0" + dia;
    }
    let param2 = ano + "-" + mes + "-" + dia;
    
    
    let promessa = fetch(`http://178.128.2.85:8080/servico/servicoAgendado/${param1}/${param2}`,
    {
        method: 'get',
        headers: {'content-type': 'application/json'}
    }).then((resposta) => {
        return resposta.json();
    });

    chamadasAPI.push(promessa);

    data.setDate(data.getDate()+1);
    dataFormatada = (data.toString()).slice(0,10);
}

Promise.all(chamadasAPI).then((dias) => {
    
    let dataLocal = new Date();
    let dataFormatadaLocal = (dataLocal.toString()).slice(0,10);
    
    dias.forEach(element => {
        for (const evento of element) {

            evento.inicioPrevisto = (evento.inicioPrevisto.toString()).substring(11,16);
            evento.fimPrevisto = (evento.fimPrevisto.toString()).substring(11,16);
            html2 += `
                <li class="single-event" data-start="${evento.inicioPrevisto}" data-end="${evento.fimPrevisto}" data-event="event-${formato}">
                    <a href="#0">
                        <em class="event-name">${evento.servicoCadastrado.tipoServico} - ${evento.servicoCadastrado.listaAnimaisServicoContratado[0].nome}</em>
                    </a>
                </li>
            `;
        }
        
        html = `
            <li class="events-group">
            <div class="top-info"><span>${dataFormatadaLocal}</span></div>
                <ul>
                    ${html2}
                </ul>
            </li>
        `;
    
        html2 = "";
        liPrincipal.innerHTML += html;
        sectionCalendario.appendChild(liPrincipal);

        dataLocal.setDate(dataLocal.getDate()+1);
        dataFormatadaLocal = (dataLocal.toString()).slice(0,10);

        formato++;
        if (formato > 4) {
        formato = 1
        }
    });

    init();
    
});



// for (let dia = 0; dia < 5; dia++) {

    


//     formato++;
//     if (formato > 4) {
//         formato = 1
//     }


    
// }


// function extrairJSON(resposta) {
//     return resposta.json();
// } 